<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var  \Laravel\Lumen\Routing\Router $router */

$router->post('user', 'UserController@register');
$router->post('authenticate', 'UserController@login');

$router->group(['prefix' => 'company'], function () use ($router) {
    $router->get('', 'CompanyController@index');
    $router->get('{id}', 'CompanyController@show');
});

$router->group(['middleware' => 'auth:api'], function () use ($router) {
    $router->get('user', 'UserController@find');

    $router->group(['prefix' => 'company'], function () use ($router) {
        $router->post('{id}/update', 'CompanyController@update');
        $router->post('', 'CompanyController@store');
        $router->delete('{id}', 'CompanyController@delete');
    });
});
