# API BACKEND - TESTE FORTBRASIL

Para realizar o desenvolvimento da api, fiz a utilização do Micro-Framework Lumen, ele baseado no Laravel e é mais leve pois não contém toda a arquitetura

## Documentação Oficial

A Documentação para o framework pode ser encontrada em [Lumen website](https://lumen.laravel.com/docs).

## Pré-Requisitos
- PHP
- Composer
- Mysql

## Instalação

- Primeiramente precisa ser feito a criação de um banco de dados MySql
- Após a criação, fazer a configuração das credenciais no arquivo .env, basta aproveitar o layout do arquivo .env.example
- Executar o comando: `composer install`
- Para finalizar, basta rodar o servidor: `php -S localhost:8000 -t public/`
- **Opcional**: para gerar dados fictícios no banco de dados, executar o comando: `php artisan migrate --seed`
