<?php

namespace App\Exceptions;

use App\Contracts\Exceptions\CustomException;
use App\Http\Responses\ApiResponse;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Prophecy\Exception\Doubler\MethodNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof NotFoundHttpException)
            return ApiResponse::error([], 'Resource not found', 404);

        if ($exception instanceof MethodNotAllowedHttpException)
            return ApiResponse::error(
                $exception->getMessage(),
                'Método HTTP não permitido',
                405);

        if ($exception instanceof MethodNotFoundException)
            return ApiResponse::error(
                $exception->getMessage(),
                'Método HTTP não encontrado',
                405);

        if ($exception instanceof ValidationException)
            return ApiResponse::error(
                $exception->errors(),
                'Parâmetros inválidos',
                400,
                true);

        if ($exception instanceof ApiTokenException)
            return ApiResponse::error(
                $exception->getMessage(),
                'Requisição não autorizada',
                401);

        if ($exception instanceof ModelNotFoundException)
            return ApiResponse::error(
                [], 'Not Found', 404);

        return parent::render($request, $exception);
    }
}
