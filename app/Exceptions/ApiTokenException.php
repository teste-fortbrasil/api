<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 12:46
 */

namespace App\Exceptions;


use App\Contracts\Exceptions\CustomException;

class ApiTokenException extends CustomException
{
    //
}
