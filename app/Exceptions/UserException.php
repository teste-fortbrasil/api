<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 11:22
 */

namespace App\Exceptions;


use App\Contracts\Exceptions\CustomException;

class UserException extends CustomException
{
    //
}
