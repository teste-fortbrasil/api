<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 13:12
 */

namespace App\Exceptions;


use App\Contracts\Exceptions\CustomException;

class CompanyException extends CustomException
{
    //
}
