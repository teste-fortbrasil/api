<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 12:04
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'document',
        'responsible',
        'phone',
        'address',
        'logo'
    ];

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = $value ? onlyNumbers($value) : null;
    }

    public function getLogoAttribute()
    {
        if (!$this->attributes['logo'])
            return null;

        return filter_var($this->attributes['logo'], FILTER_VALIDATE_URL) ?
            $this->attributes['logo'] :
            env('APP_URL') . $this->attributes['logo'];
    }
}
