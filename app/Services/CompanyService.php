<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 12:07
 */

namespace App\Services;


use App\Models\Company;
use App\Repositories\CompanyRepository;
use Illuminate\Http\UploadedFile;

class CompanyService
{
    /**
     * @var CompanyRepository
     */
    private $repository;

    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findAll(array $request = [])
    {
        return $this->repository
            ->findAll($request);
    }

    public function store(array $request): Company
    {
        $company = new Company($request);

        $company->logo = $this->uploadLogo($request['logo']);
        $company->save();

        return $company;
    }

    public function findOne(int $id)
    {
        return $this->repository->findOrFail($id);
    }

    public function update(int $id, array $request): Company
    {
        /** @var Company $company */
        $company = $this->repository->findOrFail($id);

        if (isset($request['logo']))
            $company->logo =
                $request['logo'] instanceof UploadedFile ?
                    $this->uploadLogo($request['logo']) :
                    $request['logo'];

        $company->name = $request['name'] ?? $company->name;
        $company->address = $request['address'] ?? $company->address;
        $company->responsible = $request['responsible'] ?? $company->responsible;
        $company->address = $request['address'] ?? $company->address;
        $company->phone = $request['phone'] ?? $company->phone;
        $company->save();

        return $company;
    }

    public function delete(int $id)
    {
        $company = $this->repository->findOrFail($id);

        return $company->delete();
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return string
     */
    private function uploadLogo(UploadedFile $uploadedFile)
    {
        $original_filename = $uploadedFile->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $file_ext = end($original_filename_arr);
        $destination_path = './upload/company/';
        $image = 'C-' . time() . '.' . $file_ext;

        $uploadedFile->move($destination_path, $image);

        return '/upload/company/' . $image;
    }
}
