<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 10:43
 */

namespace App\Services;


use App\Exceptions\UserException;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class UserService
{
    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function register(array $request)
    {
        $request['password'] = Hash::make($request['password']);

        return User::create($request);
    }

    public function login(array $request)
    {
        $user = $this->repository
            ->findUserByEmail($request['email']);

        if (!Hash::check($request['password'], $user->password))
            throw new UserException("Email ou Senha inválida");

        $user->api_token = Str::random();
        $user->save();

        $user->api_token = Hash::make($user->api_token);

        return $user;
    }

    public function find(User $user)
    {
        $user->setHidden(['api_token', 'password']);
        return $user;
    }
}
