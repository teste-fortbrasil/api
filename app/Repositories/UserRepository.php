<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 10:05
 */

namespace App\Repositories;


use App\Contracts\Repositories\AbstractRepository;
use App\Models\User;

class UserRepository extends AbstractRepository
{

    /**
     * AbstractRepository constructor.
     * Necessário usar o setModel()
     * para informar qual model será vinculado
     * ao Repository
     */
    public function __construct()
    {
        $this->setModel(User::class);
    }

    public function findUserByEmail(string $email)
    {
        return $this->getModel()
            ::where('email', $email)
            ->firstOrFail();
    }
}
