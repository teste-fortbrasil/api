<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 13:23
 */

namespace App\Repositories;


use App\Contracts\Repositories\AbstractRepository;
use App\Models\Company;
use Illuminate\Database\Eloquent\Builder;

class CompanyRepository extends AbstractRepository
{

    /**
     * AbstractRepository constructor.
     * Necessário usar o setModel()
     * para informar qual model será vinculado
     * ao Repository
     */
    public function __construct()
    {
        $this->setModel(Company::class);
    }

    public function findAll(array $queryRequest = [])
    {
        return $this->getModel()
            ::orderByDesc('id')
            ->when(isset($queryRequest['name']), function (Builder $query) use ($queryRequest) {
                $query->where('name', 'LIKE', "%{$queryRequest['name']}%");
            })
            ->when(isset($queryRequest['address']), function (Builder $query) use ($queryRequest) {
                $query->where('address', 'LIKE', "%{$queryRequest['address']}%");
            })
            ->when(isset($queryRequest['page']), function (Builder $query) {
                return $query
                    ->paginate();
            }, function (Builder $query) {
                return $query
                    ->get();
            });
    }
}
