<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 12:06
 */

namespace App\Http\Controllers;


use App\Http\Responses\ApiResponse;
use App\Http\Validators\CompanyRequest;
use App\Models\Company;
use App\Services\CompanyService;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    private $service;

    public function __construct(CompanyService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $result = $this->service
            ->findAll($request->query->all());

        return ApiResponse::success($result);
    }

    public function store(CompanyRequest $request)
    {
        $result = $this->service
            ->store($request->validateStore());

        return ApiResponse::success($result, "Estabelecimento criado", 201);
    }

    public function show(int $id)
    {
        $result = $this->service
            ->findOne($id);

        return ApiResponse::success($result);
    }

    public function update(int $id, CompanyRequest $request)
    {
        $result = $this->service
            ->update($id, $request->validateUpdate());

        return ApiResponse::success($result);
    }

    public function delete(int $id)
    {
        $result = $this->service
            ->delete($id);

        return ApiResponse::success($result);
    }
}
