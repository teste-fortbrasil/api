<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 10:02
 */

namespace App\Http\Controllers;


use App\Exceptions\UserException;
use App\Http\Responses\ApiResponse;
use App\Http\Validators\UserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function register(UserRequest $validator)
    {
        $request = $validator->validateRegister();

        $result = $this->service->register($request);

        return ApiResponse::success($result, "Criado com sucesso", 201);
    }

    public function login(UserRequest $validator)
    {
        $request = $validator->validateLogin();

        try {
            $result = $this->service->login($request);
        } catch (UserException $e) {
            return ApiResponse::error($e, "Erro ao fazer login");
        }

        return ApiResponse::success($result);
    }

    public function find(Request $request)
    {
        try {
            return ApiResponse::success($this->service->find($request->user()));
        } catch (\Exception $e) {
            return ApiResponse::error($e);
        }
    }
}
