<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 12:08
 */

namespace App\Http\Validators;


use App\Traits\FormRequestTrait;

class CompanyRequest
{
    use FormRequestTrait;

    public function validateStore()
    {
        return $this->validate([
            'name' => 'required|string',
            'responsible' => 'sometimes|nullable|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'logo' => 'required|image',
        ]);
    }

    public function validateUpdate()
    {
        return $this->validate([
            'name' => 'required|string',
            'responsible' => 'sometimes|nullable|string',
            'phone' => 'sometimes|string',
            'address' => 'sometimes|string',
            'logo' => 'sometimes',
        ]);
    }
}
