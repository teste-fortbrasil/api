<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 12:09
 */

if (!function_exists('onlyNumbers')) {
    function onlyNumbers(string $word)
    {
        return str_replace(['(', ')', '-', ' ', '.', '+','*','/'], '', $word);
    }
}
