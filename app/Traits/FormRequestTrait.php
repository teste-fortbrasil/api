<?php
/**
 * Created by PhpStorm.
 * User: Eduardo Roseo
 * Date: 15/08/2020
 * Time: 11:03
 */

namespace App\Traits;


use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Validator;

trait FormRequestTrait
{
    /**
     * @param array $rules
     * @return array
     * @throws ValidationException
     */
    protected function validate(array $rules): array
    {
        $validator = Validator::make(\request()->all(), $rules);

        if ($validator->errors()->any())
            throw new ValidationException($validator);

        return $validator->validated();
    }
}
